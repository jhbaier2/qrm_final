import numpy as np
import pandas as pd
from scipy.stats import norm
from scipy.optimize import newton
import os
import datetime
import matplotlib.pyplot as plt

class BlackScholesFuncs:
    
    def __init__(self):
        pass
    
    def __d1(self, s, k, r, t, sigma):
        return (np.log(s / k) + (r + sigma**2 / 2) * t) / (sigma * np.sqrt(t))
    
    def __d2(self, s, k, r, t, sigma):
        return self.__d1(s, k, r, t, sigma) - sigma * np.sqrt(t)
    
    def __vega(self, s, k, r, t, sigma):
        return s * norm.cdf(self.__d1(s, k, r, t, sigma)) * np.sqrt(t) 
    
    def call(self, s, k, r, t, sigma):
        return s * norm.cdf(self.__d1(s, k, r, t, sigma)) - k * np.exp(-r * t) * norm.cdf(self.__d2(s, k, r, t, sigma))
    
    def put(self, s, k, r, t, sigma):
        return k * np.exp(-r * t) * norm.cdf(-self.__d2(s, k, r, t, sigma)) - s * norm.cdf(-self.__d1(s, k, r, t, sigma))
    
    def newton_vol_put(self, price, s, k, r, t, sigmaGuess):
        maxIter = 10000
        tol = 1.0e-9
        sigma = sigmaGuess
        
        for i in range(maxIter):
            interPx = self.put(s, k, r, t, sigma)
            vega = self.__vega(s, k, r, t, sigma)
            
            tolArr = abs(price - interPx) < tol
            if tolArr.all():
                return sigma
            
            sigma = sigma + (price - interPx) / vega
        
        #implied vol didn't converge within 1000 iters, return best guess
        sigma = sigma/tolArr #all values that did not converge = inf
        return sigma

#########################################################################################

bs = BlackScholesFuncs()

#reading in the data
data = pd.read_csv(os.getcwd() + "\\data\\RawData.csv")
data['StrikeLN'] = np.log(data['Strike'])
disc = 0.997262
fwd = 8069.43

#other black scholes inputs
valDt = datetime.datetime(2019, 10, 28)
strikeDt = datetime.datetime(2019, 12, 20)
tenor = ((strikeDt - valDt).days - 1) / 365
rf = np.log(disc) * -1 / tenor
spot = fwd * np.exp(-rf * tenor)

#running everything through the implied vol calculator
data['ImpliedVol'] = bs.newton_vol_put(data['PutVal'], spot, data['Strike'], rf, tenor, 0.25)
print(data.head(n=3))

#we've got some bad implied vols. not sure why, will investigate later.

#plotting moneyness
moneyness = np.log(data['Strike'] / fwd)
plt.plot(data['ImpliedVol'][:len(data['Strike'])-3])
plt.show()

print('done')