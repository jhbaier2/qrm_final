function params = FitParam(strikes, prices, discount, implied_forward)
    % Arguments:
    %   strikes 
    %   prices: market put price associated with each strike
    %   discount: discount factor
    %   implied_forward
    % Outputs:
    %   params(1) = chi
    %   params(2) = nu
    %   params(3) = beta
    
    % Construct the imperical distribution, with domain as the mid points of
    % the log strikes
    F_n = zeros(1, length(strikes));
    for i=1:(length(strikes)-1)
        F_n(i) = (prices(i+1) - prices(i))/(discount*(strikes(i+1) - strikes(i)));
    end
    F_n(end) = 1;
    
    log_strikes = log(strikes); % [log(K1) log(K2) ... log(Kn)]
    k_i_dagger = (log_strikes(2:end) + log_strikes(1:end-1))/2;
    k_i_dagger = [k_i_dagger log_strikes(end)];
    
    % Fitting Student's-t distribution to imperical distribution using
    % fmincon with constraints derived in the paper
    msgid = 'MATLAB:integral:NonFiniteValue';
    warning('off', msgid);
    target_func = @(params) anderson_darling(F_n, params(1), params(2), params(3), k_i_dagger, implied_forward);
    options =  optimoptions(@fmincon, 'Algorithm', 'interior-point', 'Display','iter');
    ub = [inf, inf, -1/2-1e-10]; % upper bounds for params
    lb = [1e-10, 4, -inf]; % lower bounds for params
    
    chi = 10;
    nu = 10;
    beta = -10;
    params= fmincon(target_func, [chi, nu, beta], [], [], [], [], lb, ub, [], options);
end

function val = Skew_Student_t(x, chi, nu, beta, implied_forward)
    % PDF of the Skewed Student's-t distribution
    p = (2^(1  - nu/2) * besselk(-nu/2, sqrt(chi * (-2 * beta - 1)), 1) * exp(-sqrt(chi * (-2 * beta - 1)))) / ...
       (gamma(nu/2) * sqrt(chi * (-2 * beta - 1))^(-nu / 2));
    mu = log(implied_forward / p);
    a = 1 + (x - mu).^2 / chi;
    b = abs(beta) .* sqrt(chi) .* sqrt(a);  
    test = (gamma(nu/2 + 1/2) .* ...
          2.^(-nu/2 + 1/2) .* besselk(-nu/2 - 1/2, b, 1) .* exp(-b) .* exp(beta * (x - mu)) .* ...
          a.^(-nu/2 - 1/2)) / ...
          (gamma(nu/2) .* gamma(1/2) .* gamma(nu/2 + 1/2) .* b.^(-nu/2 - 1/2) .* sqrt(chi));
    if isnan(test) || isinf(test)
       val = 0;
    else 
       val = test;
    end
end
    
function prob = Skew_Student_t_CDF(x, chi, nu, beta, implied_forward)
    % CDF of the skewed Student's-t distribution
    lower_bound = -inf;
    prob = integral(@(z) Skew_Student_t(z, chi, nu, beta, implied_forward), lower_bound, x, "ArrayValued", true);
end

function a2 = anderson_darling(F_n, chi, nu, beta, k_i_dagger, implied_forward)
    % Anderson-Darling goodness of fit tuned to Skewed Student's-t
    a2 = (0^2 - F_n(1)^2) * log(Skew_Student_t_CDF(k_i_dagger(1), chi, nu, beta, implied_forward)) - ...
          ((1 - 0)^2 - (1 - F_n(1))^2) * log(1 - Skew_Student_t_CDF(k_i_dagger(1), chi, nu, beta, implied_forward));
    for i=2:length(F_n)
        a2 = a2 + ((F_n(i-1)^2 - F_n(i)^2) * log(Skew_Student_t_CDF(k_i_dagger(i), chi, nu, beta, implied_forward)) - ...
          ((1 - F_n(i-1))^2 - (1 - F_n(i))^2) * log(1 - Skew_Student_t_CDF(k_i_dagger(i), chi, nu, beta, implied_forward)));
    end
    a2 = -length(F_n) + length(F_n)*a2;
end