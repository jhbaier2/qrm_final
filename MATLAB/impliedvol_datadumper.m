load puts.mat % will create a variable called 'puts' in the workspace
strikes = puts(1, :);
prices = puts(2, :);
discount = 0.997262;
implied_forward = 8069.43;

begin_date = datetime("2019-10-28");
end_date   = datetime("2019-12-20");
tenor = days(end_date - begin_date - 1)/365;
r = -log(discount)/tenor;
d1 = @(S, K, r, T, sigma) (log(S./K) + (r+(sigma.^2)./2).*T)./(sigma.*sqrt(T));
d2 = @(S, K, r, T, sigma) d1(S, K, r, T, sigma) - sigma.*sqrt(T);

% define Phi (standard normal cdf)
Phi = @(x) normcdf(x, 0, 1); %normcdf(x, mu, sigma)

% define the put price formula of Black-Scholes
BS_put_price = @(S, K, r, T, sigma) K*exp(-r*T)*Phi(-d2(S, K, r, T, sigma)) - S*Phi(-d1(S, K, r, T, sigma));

market_implied_vols = zeros(1, length(strikes));

for i=1:length(strikes)
    f = @(x) BS_put_price(implied_forward, strikes(i), r, tenor, x) - prices(i);
    market_implied_vols(i) = fzero(f, 0.3);
end