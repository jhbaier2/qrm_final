function [sim_vols, market_vols] = ImpVol(chi, nu, beta, strikes, prices, discount, implied_forward)
    % Arguments:
    %  chi: argument of Skewed Student's-t
    %  nu: argument of Skewed Student's-t
    %  beta: argument of Skewed Student's-t
    %  strikes
    %  prices: market put price associated with each strike
    
    msgid = 'MATLAB:integral:NonFiniteValue';
    warning('off', msgid);
    simulated_put_prices = zeros(1, length(strikes));
    for i=1:length(strikes)
        simulated_put_prices(i) = integral(@(x) (strikes(i) - exp(x)) * Skew_Student_t(x, chi, nu, beta, implied_forward), -inf, log(strikes(i)), ...
                     "ArrayValued", true);
    end
    simulated_put_prices = simulated_put_prices * discount; % discounting to "present"
    
    %Calculating implied volatilities of market puts and simulated puts:
    begin_date = datetime("2019-10-28");
    end_date   = datetime("2019-12-20");
    tenor = days(end_date - begin_date - 1)/365;
    r_minus_q = -log(discount)/tenor;
    
    % Black-Scholes formula for puts
    d1 = @(S, K, rmq, T, sigma) (log(K./S) - (rmq - (sigma.^2)./2).*T)./(sigma.*sqrt(T));
    d2 = @(S, K, rmq, T, sigma) d1(S, K, rmq, T, sigma) - sigma.*sqrt(T);
    Phi = @(x) normcdf(x, 0, 1); %normcdf(x, mu, sigma)
    BS_put_price = @(S, K, rmq, T, sigma) K*exp(-rmq*T)*Phi(d1(S, K, rmq, T, sigma)) - S*Phi(d2(S, K, rmq, T, sigma));
    
    market_vols = zeros(1, length(strikes));
    sim_vols = zeros(1, length(strikes));
    for i=1:length(strikes)
        f = @(x) BS_put_price(implied_forward*discount, strikes(i), r_minus_q, tenor, x) - simulated_put_prices(i);
        sim_vols(i) = fzero(f, 2.1);
        f = @(x) BS_put_price(implied_forward*discount, strikes(i), r_minus_q, tenor, x) - prices(i);
        market_vols(i) = fzero(f, 2.1);
    end
end

function val = Skew_Student_t(x, chi, nu, beta, implied_forward)
    % PDF of the Skewed Student's-t distribution
    p = (2^(1  - nu/2) * besselk(-nu/2, sqrt(chi * (-2 * beta - 1)), 1) * exp(-sqrt(chi * (-2 * beta - 1)))) / ...
       (gamma(nu/2) * sqrt(chi * (-2 * beta - 1))^(-nu / 2));
    mu = log(implied_forward / p);
    a = 1 + (x - mu).^2 / chi;
    b = abs(beta) .* sqrt(chi) .* sqrt(a);  
    test = (gamma(nu/2 + 1/2) .* ...
          2.^(-nu/2 + 1/2) .* besselk(-nu/2 - 1/2, b, 1) .* exp(-b) .* exp(beta * (x - mu)) .* ...
          a.^(-nu/2 - 1/2)) / ...
          (gamma(nu/2) .* gamma(1/2) .* gamma(nu/2 + 1/2) .* b.^(-nu/2 - 1/2) .* sqrt(chi));
    if isnan(test) || isinf(test)
       val = 0;
    else 
       val = test;
    end
end