########################################
# question 2
# fit GH Skew Student T parameters via anderson darling
# parameters: chi, nu, beta
# output: csv of parameters
########################################

import numpy as np
from numpy import sqrt, exp, abs, log
from scipy.integrate import quad
from scipy.special import gamma, kv
import scipy.optimize as opt
import pandas as pd
import matplotlib.pyplot as plt


def pdf(x, chi, nu, beta, mu):
    """
    probability density function
    :param x:
    :param chi:
    :param nu:
    :param beta:
    :param mu:
    :return: pdf value
    """
    lamb = -nu / 2
    root = sqrt(1 + (x - mu) ** 2 / chi)
    inp = lamb - 0.5
    bessel = kv(inp, abs(beta) * sqrt(chi) * root)
    # if bessel is too negative it will return nan
    if np.isnan(bessel): bessel = np.finfo(float).eps

    a = gamma(-inp) / gamma(-lamb) / gamma(0.5)
    b = 2 ** (lamb + 0.5) * bessel * exp(beta * sqrt(chi) * (x - mu) / sqrt(chi))
    c = gamma(-inp) * (abs(beta) * sqrt(chi) * root) ** inp
    d = (1 + (x - mu) ** 2 / chi) ** inp / sqrt(chi)
    # if np.isnan(b).any() or b.any() > 1E9: return 0
    return a * b / c * d


def gh_skew_t(chi, nu, beta, strike, frwd, tail):
    """
    generalized hyperbolic skewed st T
    evaluates pdf up to strike
    :param chi:
    :param nu:
    :param beta:
    :param strike: upper integral bound
    :param mu: log forward
    :return: cdf value
    """
    mu = log(frwd) + log((gamma(nu/2)*(sqrt(chi*(-2*beta-1)))**(-nu/2))/ \
                         (2**(1-nu/2)*kv(-nu/2, sqrt(chi*(-2*beta-1)))))
    cdf = tail + quad(lambda k: pdf(k, chi, nu, beta, mu), 0, strike)[0]
    return cdf


def ad_test(par, fn, k, frwd):
    """
    Discrete Anderson Darling test
    :param par: (chi, nu, beta)
    :param fn: empirical distribution value
    :param k: mean log strike
    :param mu: mean, log forward
    :return: statistic value
    """
    n = len(fn)
    chi = par[0]
    nu = par[1]
    beta = par[2]
    if chi <= 0 or nu <= 4 or beta == 0:
        return np.nan
    # constraints from question 1
    if beta >= -0.5:
        return np.nan

    mu = log(frwd) + log((gamma(nu/2)*(sqrt(chi*(-2*beta-1)))**(-nu/2))/(2**(1-nu/2)*kv(-nu/2, sqrt(chi*(-2*beta-1)))))

    skewT = np.zeros(n)
    tail = quad(lambda k: pdf(k, chi, nu, beta, mu), -20, 0)[0]
    for i in range(n):
        skewT[i] = gh_skew_t(chi, nu, beta, k[i], frwd, tail)

    sum = 0
    for i in range(n):
        if i == 0:
            f = 0
        else:
            f = skewT[i - 1]
        sum += (f ** 2 - skewT[i] ** 2) * log(fn[i]) - ((1 - f) ** 2 - (1 - skewT[i]) ** 2) * log(1 - fn[i])

    return -n + n * sum


if __name__ == "__main__":
    # read empirical distribution data
    emp_dist = pd.read_csv("data/emp_dist.csv")
    fn = emp_dist.fn.values
    k_star = emp_dist.k.values
    disc = 0.997262
    forward = 8069.43
    # minimize anderson darling test
    fit = opt.minimize(lambda y: ad_test(y, fn, k_star, forward), x0=np.array([.1, 5, -10]), method='Nelder-Mead')

    print(fit)

    # chi = .0082#fit.x[0]
    # nu = 5.82#fit.x[1]
    # beta = -30.69#fit.x[2]
    # dist_fitted = np.zeros(len(k_star))
    # for i in range(len(k_star)):
    #     dist_fitted[i] = gh_skew_t(chi, nu, beta, k_star[i], forward, 0)
    #
    # plt.plot(k_star, dist_fitted)
    # plt.plot(k_star, fn)
    # plt.show()
